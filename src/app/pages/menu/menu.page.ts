import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular'; 
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  pages = [
    {
      icon: 'cart-sharp',
      title: 'Purchase',
      url: '/menu/purchase'
    },
    {
      icon: 'file-tray-full-sharp',
      title: 'Orders',
      url: '/menu/order'
    },
    {
      icon: 'person',
      title: `Profile`,
      url: '/menu/profile'
    },
  ];

  selectedPath = '';

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe(async (event: RouterEvent) => {
      if(event && event.url){
        this.selectedPath = event.url;
      }
    });
  }

}
