import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ToastService } from '../../services/toast.service';
import { Auth } from '../../models/auth';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  auth: Auth[] = [];
  constructor(private authService : AuthService,
              private toastService : ToastService,
              private storage: Storage,
              private router: Router) { }

  ngOnInit() {
  }

  async onlogIn(username, password) {
    if(username.value == ''){
      this.toastService.presentToast(`Please provide a valid username.`, `danger`);
    }else if(password.value == ''){
      this.toastService.presentToast(`Please provide valid password`, `danger`);
    }else{
      const credentials = {
        username: username.value,
        password: password.value
      };
      this.auth = await this.authService.signIn(credentials).toPromise();
      for(const data of this.auth){
        this.toastService.presentToast(data.message, `success`);
        await this.storage.set('data', data.data);
        await this.router.navigate(['menu/purchase']);
      }
    }
  }

}
