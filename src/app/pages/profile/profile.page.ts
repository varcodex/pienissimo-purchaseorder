import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  data: any;
  employeeName: string;
  employeeId: string;
  email: string;
  sectorName: string;

  constructor(private storage: Storage) { }

  async ngOnInit() {
    this.data = await this.getStorage('data');
    this.employeeName = `${this.data['user']['firstName']} ${this.data['user']['lastName']}`;
    this.employeeId = `${this.data['user']['employeeId']}`;
    this.email = `${this.data['user']['email']}`;
    this.sectorName = `${this.data['user']['sector']['name']}`;
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch(e) { console.log(e) }
  }
}
