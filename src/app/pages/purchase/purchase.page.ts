import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service'
import { SupplierService } from '../../services/supplier.service'
import { Product } from '../../models/product'
import { Supplier } from '../../models/supplier'
import { ProData } from '../../models/pro-data'
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.page.html',
  styleUrls: ['./purchase.page.scss'],
})
export class PurchasePage implements OnInit {
  products: Product[] = [];
  supplier: Supplier[] = []
  proData: ProData [] = [];
  supplierId = 0;
  subSectorId = 0;
  data: any;
  currentPage = 1;
  constructor(private productService : ProductService,
              private supplierService : SupplierService,
              private storage: Storage) { }

  ngOnInit() {
    this.getProducts(false, "");
    this.getSupplier();
  }

  async getSupplier(){
    const credentials= {
      "all": true
    };
    this.data = await this.getStorage('data');
    this.supplier = await this.supplierService.getSupplier(credentials, this.data['token']).toPromise();
  }

  async getProducts(isFirstLoad, event) {
    if(!isFirstLoad){
      this.proData = [];
      this.products = [];
      this.currentPage = 1;
      const credentials= {
        pageSize:10,
        currentPage: this.currentPage,
        search:'',
        supplierId: this.supplierId,
        subSectorId: this.subSectorId,
        status: -1
      };
      this.data = await this.getStorage('data');
      this.products = await this.productService.getProducts(credentials, this.data['token']).toPromise();
      this.proData= this.proData.concat(this.products['data']);
    }else{
     if( this.products['totalPages'] > this.currentPage){
       this.currentPage = this.currentPage + 1
        const credentials= {
          pageSize:10,
          currentPage: this.currentPage,
          search:'',
          supplierId: this.supplierId,
          subSectorId: this.subSectorId,
          status: -1
        };
        this.data = await this.getStorage('data');
        this.products = await this.productService.getProducts(credentials, this.data['token']).toPromise();
        this.proData= this.proData.concat(this.products['data']);
        if (isFirstLoad)
              event.target.complete();
     }else{
      if (isFirstLoad)
              event.target.complete();
     }
    }
  }

  doInfinite(event) {
    this.getProducts(true, event);
  }

  supplierChange($event) {
    this.supplierId = $event.target.value;
    this.getProducts(false, "");
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch(e) { console.log(e) }
  }

}
