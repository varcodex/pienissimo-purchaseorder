import { Injectable } from '@angular/core';
import { Supplier } from '../models/supplier'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  public headers:Headers; 
  constructor(private http: HttpClient) { }
  
  getSupplier(credentials, token) : Observable<Supplier[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        'Authorization': `Bearer ${token}`
      })
    }
    return this.http.post<Supplier[]>(`${environment.api}supplier/list`, JSON.stringify(credentials), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  errorHandl(error) {
    let errorMessage = '';
    console.log(error.error)
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.message}`;
    }
    alert(errorMessage);
    return throwError(errorMessage);
  }
}
