export interface Auth {
    data: {
        user: {
            id: number,
            email: string,
            password: string,
            employeeId: string,
            firstName: string,
            lastName: string,
            role: number,
            status: number,
            sector: {
                id: number,
                name: string,
                status: number,
                approver: number,
                createAt: string,
                updateAt: string,
                deleteAt: string
            },
            createAt: string,
            updateAt: string,
            deleteAt: string
        },
        token: string
    },
    title; string,
    message: string
}
