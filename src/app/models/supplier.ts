export interface Supplier {
    id: number,
    name: string,
    contactPerson: string,
    telNo: string,
    mobileNo: string,
    status: number,
    createAt: string,
    updateAt: string,
    deleteAt: string
}
