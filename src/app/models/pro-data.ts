export interface ProData {
    id: number,
    code: string,
    supplier: {
        id: number,
        name: string,
        contactPerson: string,
        telNo: string,
        mobileNo: string,
        status: number,
        createAt: string,
        updateAt: string,
        deleteAt: string
    },
    subSector: {
        id: number,
        name: string,
        status: number,
        createdAt: string,
        deletedAt: string,
        updatedAt: string
    },
    name: string,
    description: string,
    sku: string,
    unitMeasure: string,
    price: number,
    status: number,
    createAt: string,
    updateAt: string,
    deleteAt: string
    
}
